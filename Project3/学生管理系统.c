#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>

#define M 20
#define G 20
#define F 20

struct stu
{
	int number;
	char name[M];
	char sex[F];
	int age;
	float math;
	float Chinese;
	float English;
	float total_score;
}student[G];

//函数的声明
void menu();
void input_message();
void delete_message();
void search_message();
void modify_message();
void show_message();

int sum = 0;  //表示录入信息的人数

int main()
{
	while (1)
	{
		system("cls");
		menu();

		char ch = _getch();
		{
			switch (ch)
			{
			case '1':
				input_message();
				break;
			case '2':
				delete_message();
				break;
			case '3':
				modify_message();
				break;
			case '4':
				search_message();
				break;
			case '5':
				show_message();
				break;
			case '0':
				return 0;
				break;

			default:
				printf("请在0—5中选择\n");
				break;
			}
		}
		system("pause");
	}
	return 0;
}

void menu()
{
	printf("***********************欢迎使用学生管理系统(当前共有%d名学生)***********************\n", sum);
	printf("*                                                                                  *\n");
	printf("*                       -----------------------------------                        *\n");
	printf("*                       |      Powered By 软件工程系      |                        *\n");
	printf("*                       | 郑州工程技术学院   信息工程学院 |                        *\n");
	printf("*                       |   第四组  刘昌昕 郭峻成 崔童桐  |                        *\n");
	printf("*                       |           李娇怡  刘备          |                        *\n");
	printf("*                       -----------------------------------                        *\n");
	printf("*                                                                                  *\n");
	printf("*                       1.添加学生信息                                             *\n");
	printf("*                       2.删除学生信息（根据学号）                                 *\n");
	printf("*                       3.修改学生信息（根据学号）                                 *\n");
	printf("*                       4.查询学生信息                                             *\n");
	printf("*                       5.显示所有学生信息以及统计信息                             *\n");
	printf("*                       0.退出软件                                                 *\n");
	printf("*                                                                                  *\n");
	printf("************************************************************************************\n");
}

void input_message()
{
	int i;
	int n;

	i = sum; // 结束循环后，能够再次进入

	while (i < sum + 1)
	{
		n = 1;

		while (n)
		{
			n = 0;
			printf("输入学号：");
			scanf_s("%d", &student[i].number);
			printf("输入姓名：");
			scanf_s("%s", student[i].name, M);
			printf("输入性别：");
			scanf_s("%s", &student[i].sex, F);
			printf("输入年龄：");
			scanf_s("%d", &student[i].age);
			printf("数学成绩：");
			scanf_s("%f", &student[i].math);
			printf("语文成绩：");
			scanf_s("%f", &student[i].Chinese);
			printf("英语成绩：");
			scanf_s("%f", &student[i].English);

			for (int j = 0; j < i; j++)
			{
				if (student[i].number == student[j].number)  //判断是否重复
				{
					printf("错误：该学生信息已存在，无需重复添加！");

					return;    //退出函数
					n = 1;
					break;
				}
			}
		}

		if (n == 0)
		{
			printf("提示：添加成功\n");
			i++;
		}
	}
	++sum;
}

void delete_message()
{
	int i = 0;
	int arr1;

	printf("输入学号：");
	scanf_s("%d", &arr1);

	for (i = 0; i < sum; i++)
	{
		if (student[i].number == arr1)
		{
			if (i == sum - 1)                       //判断是不是数组最后一个元素 
			{
				printf("提示：删除成功，");
				sum--;
				return;
			}
			else
			{
				for (int j = i; j < sum - 1; j++)
				{
					student[j] = student[j + 1];    // 赋值删除
				}
				printf("提示：删除成功，");
				sum--;
				return;  // 退出函数
			}
		}
	}

	printf("错误：该学生信息不存在，删除失败！");
}

void modify_message()
{
	int ID;
	printf("输入学号:");
	scanf_s("%d", &ID);

	for (int i = 0; i < sum; ++i)
	{
		if (ID == student[i].number)
		{
			printf("输入姓名：");
			scanf_s("%s", student[i].name, M);
			printf("输入性别：");
			scanf_s("%s", &student[i].sex, F);
			printf("输入年龄：");
			scanf_s("%d", &student[i].age);
			printf("数学成绩：");
			scanf_s("%f", &student[i].math);
			printf("语文成绩：");
			scanf_s("%f", &student[i].Chinese);
			printf("英语成绩：");
			scanf_s("%f", &student[i].English);

			printf("提示：修改成功。");
			system("pause");
		}
		else if (i + 1 == sum)
		{
			printf("错误：该学生信息不存在，修改失败！");
		}
	}
}

void search_message()
{
	system("cls");
	printf("************查询学生成绩（当前共有%d名学生）************************\n", sum);
	printf("*                                                                 *\n");
	printf("*             1.根据学号查询                                      *\n");
	printf("*             2.根据姓名查询                                      *\n");
	printf("*             3.根据数学成绩查询                                  *\n");
	printf("*             4.根据语文成绩查询                                  *\n");
	printf("*             5.根据英语成绩查询                                  *\n");
	printf("*             6.根据总成绩查询                                    *\n");
	printf("*             0.返回主菜单                                        *\n");
	printf("*                                                                 *\n");
	printf("*******************************************************************\n");

	char p = _getch();
	switch (p)
	{
	case '1':
	{
		printf("输入学号：");
		int arr2;
		scanf_s("%d", &arr2);
		int count = 0;

		for (int i = 0; i < sum; i++)
		{
			if (student[i].number == arr2)
			{
				student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);

				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				printf("----------------------------------------------------------------------\n");
				++count;
				system("pause");
				system("cls");
				return search_message();
			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}
		printf("共查询%d条记录\n", count);

		break;
	}
	case '2':
	{
		printf("输入姓名：");
		char arr3[20];
		scanf_s("%s", arr3, 20);
		int count = 0;
		for (int i = 0; i < sum; i++)
		{
			if (strcmp(student[i].name, arr3) == 0)
			{
				student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				printf("----------------------------------------------------------------------\n");
				++count;
				system("pause");
				system("cls");
				return search_message();
			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}
		printf("共查询%d条记录\n", count);
		break;
	}
	case '3':
	{
		int count = 0;       //查询的条数
		float a, b;
		printf("要查询的数学成绩的范围：");
		scanf_s("%f%f", &a, &b);

		for (int i = 0; i < sum; i++)
		{
			if (student[i].math >= a && student[i].math <= b)
			{
				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				break;
			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}

		for (int i = 0; i < sum; i++)
		{
			if (student[i].math >= a && student[i].math <= b)
			{
				student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				++count;
			}
		}
		printf("----------------------------------------------------------------------\n");
		printf("共查询%d条记录\n", count);

		system("pause");
		system("cls");
		return search_message();
		break;
	}
	case '4':
	{
		int count = 0;
		float c, d;
		printf("要查询的语文成绩的范围：");
		scanf_s("%f%f", &c, &d);

		for (int i = 0; i < sum; i++)
		{
			if (student[i].Chinese >= c && student[i].Chinese <= d)
			{
				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				break;
			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}

		for (int i = 0; i < sum; i++)
		{
			if (student[i].Chinese >= c && student[i].Chinese <= d)
			{
				student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				++count;
			}
		}
		printf("----------------------------------------------------------------------\n");
		printf("共查询%d条记录\n", count);

		system("pause");
		system("cls");
		return search_message();
		break;

	}
	case '5':
	{
		int count = 0;
		float e, f;
		printf("要查询的英语成绩的范围：");
		scanf_s("%f%f", &e, &f);

		for (int i = 0; i < sum; i++)
		{
			if (student[i].English >= e && student[i].English <= f)
			{
				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				break;

			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}
		for (int i = 0; i < sum; i++)
		{
			if (student[i].English >= e && student[i].English <= f)
			{
				student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				++count;
			}
		}
		printf("----------------------------------------------------------------------\n");
		printf("共查询%d条记录\n", count);

		system("pause");
		system("cls");
		return search_message();
		break;

	}
	case '6':
	{
		float g, h;
		int count = 0;
		printf("要查询的总成绩的范围：");
		scanf_s("%f%f", &g, &h);

		for (int i = 0; i < sum; i++)
		{
			student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
			if (student[i].total_score >= g && student[i].total_score <= h)
			{
				printf("----------------------------------------------------------------------\n");
				printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
				printf("----------------------------------------------------------------------\n");
				break;
			}
			else if (i + 1 == sum)
			{
				printf("查无此人");

				system("pause");
				system("cls");
				return search_message();
			}
		}

		for (int i = 0; i < sum; i++)
		{
			student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
			if (student[i].total_score >= g && student[i].total_score <= h)
			{
				printf(" %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
					student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
					student[i].Chinese, student[i].English, student[i].total_score);
				++count;
			}
		}
		printf("----------------------------------------------------------------------\n");
		printf("共查询%d条记录\n", count);

		system("pause");
		system("cls");
		return search_message();
		break;
	}
	case '0':
		system("cls");
		return menu();
		break;

	default:
		printf("请在0—6中选择\n");
		system("pause");
		system("cls");
		return search_message();
		break;
	}
}
void show_message()
{
	system("cls");

	float  m_score = 0, C_score = 0, E_score = 0; // 数学，语文，英语 各自的平均成绩
	float t_score = 0; // 总成绩的平均成绩
	float t_age = 0;
	const char arr4[3] = "男";
	int  boy = 0; //统计男生总人数 
	int girl = 0; //统计女生总人数
	int count = 0;// 统计信息条数

	for (int i = 0; i < sum; i++)
	{
		if (strcmp(arr4, student[i].sex) == 0)
		{
			++boy;
		}
		else
		{
			++girl;
		}

		m_score += student[i].math;     //数学总成绩
		C_score += student[i].Chinese;  // 语文总成绩
		E_score += student[i].English; // 英语总成绩
		t_age += student[i].age;       // 总年龄
		student[i].total_score = (student[i].math + student[i].English + student[i].Chinese);
		t_score += student[i].total_score;  // 总成绩之和
	}
	m_score = m_score / sum;
	C_score = C_score / sum;
	E_score = E_score / sum;
	t_age = t_age / sum;
	t_score = t_score / sum;

	if (sum == 0)
	{
		printf("----------------------------------------------------------------------\n");
		printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
		printf("----------------------------------------------------------------------\n");
		printf("  --\t|--\t|0/0\t|无\t|无\t|无\t|无\t|无\t\n");
		printf("----------------------------------------------------------------------\n");
		printf("共查询0条记录\n");
		printf("请输入学生信息\n");
	}
	if (sum != 0)
	{
		printf("----------------------------------------------------------------------\n");
		printf(" 学号\t|姓名\t|性别\t|年龄\t|数学\t|语文\t|英语\t|总成绩\t\n");
		printf("----------------------------------------------------------------------\n");
		for (int i = 0; i < sum; i++)
		{
			printf("  %d\t|%s\t|%s\t|%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
				student[i].number, student[i].name, student[i].sex, student[i].age, student[i].math,
				student[i].Chinese, student[i].English, student[i].total_score);
			++count;
		}
		printf("----------------------------------------------------------------------\n");
		printf("  --\t|--\t|%d/%d\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t|%.2f\t\n",
			boy, girl, t_age, m_score, C_score, E_score, t_score);
		printf("----------------------------------------------------------------------\n");
		printf("共查询%d条记录\n", count);
	}
}

