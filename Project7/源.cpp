#include<stdio.h>
#include<stdlib.h>
#include<graphics.h>
#include<mmsystem.h>
#pragma comment(lib, "winmm")
#include"tools.hpp"

#define WIDTH 1080
#define HEIGHT 720
                              //注意：双缓冲区也可以用来，切换已经画好图形的界面，贴出新的界面
#include<string.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<windows.h>
#include<process.h>

// 距离公式
#define DISTANCE(hook) sqrt(pow(hook->x - hook->end_x, 2)+pow(hook->y - hook->end_y, 2))


int pass = 1; //过关数
int target_goal = 400; //初始分数

// 封装角色
enum Situ { Default, notFinish, Win, Fail, Exit, End };

typedef enum Mode
{
	Routine,
	Challenge,
	Endless,
} mode;

struct Role
{
	int x;
	int y;
	int height;
	int width;
	int Mode;
	int score;
	int situ;
} role;

typedef struct button
{
	int x, y; //左上坐标
	int xx, yy; //右下坐标

	COLORREF color0; //COLORREF  接受RGB函数的类型   鼠标没选中
	COLORREF color1;  //鼠标选中
	char* buttonStr;
}BTN,*LPBTN;

//封装矿石
// 
//图片定义
IMAGE BG, bg; //菜单背景和游戏背景
IMAGE ROLE;
IMAGE mine1, mine2, mine3; //三种矿物
IMAGE* mineTypes[3] = { &mine1, &mine2, &mine3 }; //指针数组
IMAGE GOLE;
IMAGE TIME;
IMAGE WIN;
IMAGE FAIL;

int mineNum = 12; //一局中最大的矿石数量
enum mineType{mine_1, mine_2, mine_3, types};

struct Mine
{
	int x;
	int y;
	int height;
	int width;
	int type;
	int price;// 价值
	bool isCatch;//判断是否抓住

}mines[40];

//封装钩子
//enum Dir {Left, Right};
//enum State { Normal, Long, Short};
//
//#define MAX_ANGLE 80
// 
//struct Hook
//{
//	double x, y; //起始点坐标
//	double len; //长
//	double end_x, end_y; //结束坐标
//	int state;      //钩子的状态
//	int Dir;     //钩子摆动的方向
//	double dx, dy;  //伸长时的变化量
//	int index;  // 抓取矿石的标识下标
//	int angle;  //角度
//
//} hook;

enum Dir { Left, Right };
enum State { Normal, Long, Short };

#define MAX_ANGLE 90
struct Hook
{
	double x, y;	// 起始点坐标
	double len;		// 长度
	double end_x, end_y;	// 结束点坐标
	double angle;	// 与初始方向所成角度
	int state;		// 钩子所处状态
	int Dir;		// 钩子摆动方向
	double dx, dy;	// 伸长时的变化量
	int index;		// 抓取到的矿物的标识
} hook;

//绘制按钮
LPBTN createButton(int x, int y, int xx, int yy, COLORREF color0, COLORREF color1, const char* buttonStr);
//绘制普通按钮(没有选中)  
void draw_button0(LPBTN pButton);
//绘制选中的按钮
void draw_button1(LPBTN pButton);

//绘制游戏主界面
void mainMenu();

//音乐状态
bool music_state = true;
char btn[15] = "音乐：开";
//音乐绘制
void music_switch(LPBTN pButton);

void loadImage();
void select_menu(); //选择界面
void draw_game();
void draw_game_graphics();

//绘制角色
void init_role(Role* role, int x, int y);
void draw_role(Role* role);

//绘制矿石
void init_mine(Mine* mine, int x, int y, int type);
void draw_mine(Mine* mine);

//绘制钩子
void init_hook(Hook* hook, int x, int y);
void draw_hook(Hook* hook);
void hook_swing(Hook* hook, double angleInc);
void hook_control(Hook* hook, double speed);
void hook_catch(Hook* hook);

#define PI 3.14 //π

//所有的初始化
void  init();

//判断游戏分数关卡重置）
void Judge(Role* role);

void draw_win();
void draw_fail();
void draw_end();

//模式三的分数
int model3_score = 0;

//倒计时实现
#define LEFT_TIME 21;
int left_time = LEFT_TIME;
int mode3_score = 0;

int* arg = &left_time; //倒计时线程需要

//倒计时，(用多线程实现倒计时)
void countDown(void * pArg);
void draw_time(int t );   //倒计时绘制


void rule_menu();
void story_menu();

int main()
{
	mainMenu();
	return 0;
}

void loadImage()
{
	//加载背景图片
	loadimage(&BG, "./src/img/menu_bg.png", WIDTH, HEIGHT);
	loadimage(&bg, "./src/img/bg.jpg", WIDTH, HEIGHT);

	//加载角色
	loadimage(&ROLE, "./src/img/role.png", 131, 186);

	//加载矿石
	loadimage(&mine1, "./src/img/mine_1.png", 50, 50);
	loadimage(&mine2, "./src/img/mine_2.png", 65, 56);
	loadimage(&mine3, "./src/img/mine_3.png", 40, 40);

	//加载分数和时间
	loadimage(&GOLE, "./src/icon/goal.png", 40, 40);
	loadimage(&TIME, "./src/icon/time.png", 40, 40);

	//加载游戏胜利和失败
	loadimage(&WIN, "./src/img/win.jpg", WIDTH, HEIGHT);
	loadimage(&FAIL, "./src/img/fail.jpg", WIDTH, HEIGHT);

}


//绘制游戏主界面
void mainMenu()
{
menu:; //goto 语句
select1:;

	loadImage();
	
	initgraph(WIDTH, HEIGHT);
	putimage(0, 0, &BG);

	settextstyle(70, 0, "华文彩云");//第一个参数指定高度，第二个平均宽度(如果为0,自适应)
	setbkmode(TRANSPARENT);
	setcolor(RGB(110, 113, 210));
	outtextxy(428, 220, "坤坤寻球记");
	


	// 创建按钮
	LPBTN play = createButton(0, 113, 128, 141, RGB(144, 150, 239), RGB(104, 110, 199), "开始游戏");
	LPBTN rank = createButton(0, 255, 128, 285, RGB(156, 210, 238), RGB(116, 170, 198), "规则");

	if (music_state)
	{
		strcpy(btn, "音乐：开"); 
	}
	else 
	{
		strcpy(btn, "音乐：关"); 
	}

	LPBTN music = createButton(107, 320, 201, 380, RGB(156, 210, 238), RGB(116, 170, 198), btn);
	LPBTN team = createButton(211, 414, 306, 474, RGB(156, 210, 238), RGB(116, 170, 198), "背景故事");
	LPBTN exit1 = createButton(315, 508, 412, 568, RGB(156, 210, 238), RGB(116, 170, 198), "退出游戏");

	// 按钮绘制
	draw_button0(play);
	draw_button0(rank);
	draw_button0(music);
	draw_button0(team);
	draw_button0(exit1);

	music_state = false;
	music_switch(music);

	ExMessage m;  //定义消息

	
	while(1)
	{
		BeginBatchDraw();

		m = getmessage(EM_MOUSE); //接收鼠标消息 (鼠标坐标m.x m.y)

		//开始进行按钮交互
		if (m.x >= play->x && m.x <= play->xx && m.y <= play->yy && m.y >= play->y)
		{
			setfillcolor(play->color1);
			//POINT easyx库中一个类 包含(x, y)
			//solidploygon 用来画无边框的填充多边形

			POINT pts1[] = { {0, 0}, {0, 141}, {128, 113} };
			solidpolygon(pts1, 3);	// 上三角
			//rectangle(0, 141, 128, 255);
			POINT pts2[] = { {0, 141}, {128, 141}, {128,113} };
			solidpolygon(pts2, 3);	// 下三角(中间部分)

			POINT pts3[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts3, 3);	// 下三角

			draw_button1(play);

			if (m.lbutton == true)  //Indicates whether the left mouse button is pressed
			{
				select_menu();//进入选模式页面
				goto menu;
			}
		}
		else
		{
			setfillcolor(play->color0);
			
			POINT pts1[] = { {0, 0}, {0, 141}, {128, 113} };
			solidpolygon(pts1, 3);	// 上三角
			//rectangle(0, 141, 128, 255);
			POINT pts2[] = { {0, 141}, {128, 141}, {128,113} };
			solidpolygon(pts2, 3);	// 下三角(中间部分)
			
			POINT pts3[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts3, 3);	// 下三角

			draw_button0(play);

		}

		if (m.x >= rank->x && m.x <= rank->xx && m.y >= rank->y && m.y <= rank->yy)
		{
			setfillcolor(rank->color1);
			POINT pts1[] = { {0, 141}, {0, 255}, {128, 255} };
			solidpolygon(pts1, 3);	// 上三角
			solidrectangle(0, 255, 128, 286); //中间矩形
			POINT pts2[] = { {128, 255}, {128, 286}, {160, 286} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(rank);

			if (m.lbutton == true)
			{
				rule_menu();//规则界面
			}

		}
		else {
			setfillcolor(rank->color0);
			POINT pts1[] = { {0, 141}, {0, 255}, {128, 255} };
			solidpolygon(pts1, 3);	// 上三角
			solidrectangle(0, 255, 128, 286); //中间矩形
			POINT pts2[] = { {128, 255}, {128, 286}, {160, 286} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(rank);
		}

		// 音乐
		if (m.x >= music->x && m.x <= music->xx && m.y >= music->y && m.y <= music->yy)
		{
			setfillcolor(music->color1);
			POINT pts1[] = { {39, 320}, {107, 320}, {107, 380} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(107, 320,201 ,380); //中间矩形
			POINT pts2[] = { {201, 320}, {201, 380}, {269, 380} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(music);
			if (m.lbutton == true)
			{
				music_switch(music);
			}
		}
		else {
			setfillcolor(music->color0);
			POINT pts1[] = { {39, 320}, {107, 320}, {107, 380} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(107, 320, 201, 380); //中间矩形
			POINT pts2[] = { {201, 320}, {201, 380}, {269, 380} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(music);
		}

		if (m.x >= team->x && m.x <= team->xx && m.y >= team->y && m.y <= team->yy)
		{
			setfillcolor(team->color1);
			POINT pts1[] = { {144, 414}, {211, 414}, {211, 474} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(211, 414, 306,474);
			POINT pts2[] = { {306, 414}, {306, 474}, {373, 474} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(team);
			if (m.lbutton == true)
			{
				story_menu();//故事界面
			}
		}
		else {
			setfillcolor(team->color0);
			POINT pts1[] = { {144, 414}, {211, 414}, {211, 474} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(211, 414, 306, 474);
			POINT pts2[] = { {306, 414}, {306, 474}, {373, 474} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(team);
		}
		if (m.x >= exit1->x && m.x <= exit1->xx && m.y >= exit1->y && m.y <= exit1->yy) {
			setfillcolor(exit1->color1);
			POINT pts1[] = { {250, 508}, {315, 508}, {315, 568} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {412, 508}, {412, 568}, {477, 569} };
			solidrectangle(315, 508, 412, 569);
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(exit1);

			if (m.lbutton == true) 
			{
				exit(0);
			}
		}

		else 
		{

			setfillcolor(exit1->color0);
			POINT pts1[] = { {250, 508}, {315, 508}, {315, 568} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {412, 508}, {412, 568}, {477, 569} };
			solidrectangle(315, 508, 412, 569);// 中间矩形
			solidpolygon(pts2, 3);	// 右三角

			draw_button0(exit1);
		}

		EndBatchDraw();
	}

}


//绘制按钮
LPBTN createButton(int x, int y, int xx, int yy, COLORREF color0, COLORREF color1, const char* buttonStr)
{
	LPBTN button = (LPBTN)malloc(sizeof(BTN));
	button->x = x;
	button->xx = xx;
	button->y = y;
	button->yy = yy;
	button->color0 = color0;
	button->color1 = color1;
	//注意:给buttonStr开空间
	button->buttonStr = (char*)malloc(sizeof(strlen(buttonStr) + 1));

	strcpy(button->buttonStr, buttonStr);
	
	return button;
}

//绘制普通按钮
void draw_button0(LPBTN pButton)
{
	setfillcolor(pButton->color0);
	settextstyle(22,0, "幼圆", 0, 0, 20, false, false, false);
	settextcolor(RGB(0, 119, 185));
	setbkmode(TRANSPARENT);

	//按钮的文字居中
	//outtextxy();
	outtextxy(pButton->x + ((pButton->xx - pButton->x) - textwidth(pButton->buttonStr)) / 2, pButton->y + (pButton->yy - pButton->y - 22) / 2, pButton->buttonStr);

}

//绘制选中的按钮
void draw_button1(LPBTN pButton)
{
	setfillcolor(pButton->color0);
	settextstyle(22, 0, "幼圆", 0, 0, 20, false, false, false);
	settextcolor(YELLOW);
	//setbkmode(TRANSPARENT);

	//按钮的文字居中
	//outtextxy(pButton->x + ((pButton->xx - pButton->x - textwidth(pButton->buttonStr)) >> 1), ((pButton->y + pButton->yy - pButton->y - textwidth(pButton->buttonStr)) >> 1), pButton->buttonStr);
	outtextxy(pButton->x + ((pButton->xx - pButton->x) - textwidth(pButton->buttonStr)) / 2, pButton->y + (pButton->yy - pButton->y - 22) / 2, pButton->buttonStr);
}

void music_switch(LPBTN pButton)
{

	
	if (!music_state)
	{
		mciSendString("open ./src/sound/BGM.mp3", NULL, 0, NULL);
		mciSendString("play ./src/sound/BGM.mp3 repeat", NULL, 0, NULL);  //注意：这里使用 repeat实现音乐的循环播放
		strcpy(pButton->buttonStr, "音乐：开");
		music_state = true;
	}
	else
	{
		mciSendString("stop  ./src/sound/BGM.mp3", NULL, 0, NULL);
		strcpy(pButton->buttonStr, "音乐：关");
		music_state = false;
	}
}

void select_menu()
{


	loadImage();

	initgraph(1080, 720);
	putimage(0, 0, &BG);
	// 设置字体
	settextstyle(70, 0, "华文彩云");
	settextcolor(RGB(110, 113, 210));
	setbkmode(TRANSPARENT);
	// 输出文字
	outtextxy(428, 220, "模式选择");

	// 创建按钮
	LPBTN exit = createButton(0, 113, 128, 141, RGB(144, 150, 239), RGB(104, 110, 199), "返回菜单");
	LPBTN mode1 = createButton(107, 320, 201, 380, RGB(156, 210, 238), RGB(116, 170, 198), "常规模式");
	LPBTN mode2 = createButton(211, 414, 306, 474, RGB(156, 210, 238), RGB(116, 170, 198), "挑战模式");
	LPBTN mode3 = createButton(315, 508, 412, 568, RGB(156, 210, 238), RGB(116, 170, 198), "无尽模式");

	// 按钮绘制
	draw_button0(exit);
	draw_button0(mode1);
	draw_button0(mode2);
	draw_button0(mode3);

	ExMessage m;
	while (true)
	{

		// 获取鼠标消息
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		// 返回交互
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {0, 0}, {0, 113}, {128, 113} };
			solidpolygon(pts1, 3);	// 上三角
			solidrectangle(0, 113, 128, 141); //中间矩形
			POINT pts2[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts2, 3);	// 下三角
			draw_button1(exit);
			if (m.lbutton == true) 
			{
				return;
			}
		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {0, 0}, {0, 113}, {128, 113} };
			solidpolygon(pts1, 3);
			solidrectangle(0, 113, 128, 141); //中间矩形
			POINT pts2[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts2, 3);
			draw_button0(exit);
		}

		// mode1交互
		if (m.x >= mode1->x && m.x <= mode1->xx && m.y >= mode1->y && m.y <= mode1->yy)
		{
			setfillcolor(mode1->color1);
			POINT pts1[] = { {39, 320}, {107, 320}, {107, 380} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(107, 320, 201, 380); //中间矩形
			POINT pts2[] = { {201, 320}, {201, 380}, {269, 380} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(mode1);
			if (m.lbutton == true)
			{
				role.Mode = Routine;
				draw_game();
				
				/*role.Mode = Routine;
				draw_game();
				goto select;*/
			}
		}
		else {
			setfillcolor(mode1->color0);
			POINT pts1[] = { {39, 320}, {107, 320}, {107, 380} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(107, 320, 201, 380); //中间矩形
			POINT pts2[] = { {201, 320}, {201, 380}, {269, 380} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(mode1);
		}
		// mode2 交互
		if (m.x >= mode2->x && m.x <= mode2->xx && m.y >= mode2->y && m.y <= mode2->yy)
		{
			setfillcolor(mode2->color1);
			POINT pts1[] = { {144, 414}, {211, 414}, {211, 474} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(211, 414, 306, 474);
			POINT pts2[] = { {306, 414}, {306, 474}, {373, 474} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(mode2);

			if (m.lbutton == true)
			{
				role.Mode = Challenge;
				draw_game();
				//goto select;*/
			}
		}
		else {
			setfillcolor(mode2->color0);
			POINT pts1[] = { {144, 414}, {211, 414}, {211, 474} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(211, 414, 306, 474);
			POINT pts2[] = { {306, 414}, {306, 474}, {373, 474} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(mode2);
		}
		// mode3 交互
		if (m.x >= mode3->x && m.x <= mode3->xx && m.y >= mode3->y && m.y <= mode3->yy) {
			setfillcolor(mode3->color1);
			POINT pts1[] = { {250, 508}, {315, 508}, {315, 568} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(315, 508, 412, 569);
			POINT pts2[] = { {412, 508}, {412, 568}, {477, 569} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button1(mode3);
			if (m.lbutton == true)
			{
				role.Mode = Endless;
				draw_game();
				//goto select;
			}
		}
		else {
			setfillcolor(mode3->color0);
			POINT pts1[] = { {250, 508}, {315, 508}, {315, 568} };
			solidpolygon(pts1, 3);	// 左三角
			solidrectangle(315, 508, 412, 569);
			POINT pts2[] = { {412, 508}, {412, 568}, {477, 569} };
			solidpolygon(pts2, 3);	// 右三角
			draw_button0(mode3);
		}

		EndBatchDraw();
	}

}

void draw_game()
{
	//游戏不是常规模式
	if (role.Mode != Routine)
	{
		_beginthread(countDown, 0, (void*)arg);
	}

	draw_game_graphics();
}

void draw_game_graphics()
{
	initgraph(WIDTH, HEIGHT);

newgame:;
	init();
	


	// 创建按钮
	LPBTN exit = createButton(0, 570, 160, 620, RGB(226, 210, 211), RGB(186, 170, 179), "返回");
	if (music_state) { strcpy(btn, "音乐：开"); }
	else { strcpy(btn, "音乐：关"); }
	LPBTN music = createButton(0, 630, 160, 680, RGB(226, 210, 211), RGB(186, 170, 179), btn);

	draw_button0(exit);
	draw_button0(music);

	while (1)	
	{
		BeginBatchDraw();

		putimage(0, 0, &bg);
		draw_role(&role);
		draw_hook(&hook);
		draw_mine(mines);
	
		ExMessage m;
		peekmessage(&m, EX_MOUSE, true);

		if (role.Mode != Normal)
		{
			draw_time(left_time);
		}
		//注意:getmessage和peekmessage的区别,getmessage的会把程序卡在哪里，
		//然后有消息了才会往下走，peek有消息的就接收，没消息就略过


		// 按键交互 返回
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			
			setfillcolor(music->color1);
			solidrectangle(0, 570, 160, 620);
			draw_button1(exit);

			if (m.lbutton == true)
			{
				role.situ = Exit;
			}
		}

		else
		{
			setfillcolor(music->color0);
			solidrectangle(0, 570, 160, 620);
			draw_button0(exit);
		}

		if (m.x >= music->x && m.x <= music->xx && m.y >= music->y && m.y <= music->yy)
		{
			setfillcolor(music->color1);
			solidrectangle(0, 630, 160, 680);

			draw_button1(music);

			if (m.lbutton == true)
			{

			//解决物理按键影响的方法
			while (m.lbutton == true)
			{
				peekmessage(&m, EX_MOUSE, true);
			}
				music_switch(music);
			}
		}
		else
		{
			setfillcolor(music->color0);
			solidrectangle(0, 630, 160, 680);

			draw_button0(music);
		}



		FlushBatchDraw();

		hook_swing(&hook, 0.08);
		hook_control(&hook, 0.7);
		hook_catch(&hook);

		Judge(&role);

		EndBatchDraw();


		if (role.situ == Exit)
		{
			pass = 1;
			target_goal = 400;
			left_time = LEFT_TIME;

			//前面不用goto语体跳出，这里就只能直接返回主界面，不然会出现界面冲突
			return mainMenu(); 

		}

		if (role.situ == notFinish)
		{
			role.situ = Default;
			goto newgame;
		}

		if (role.situ == Win)
		{
			draw_win();
			return;
		}

		if (role.situ == End)
		{
			draw_end();
			return;
			
		}

		if (role.situ == Fail)
		{
			draw_fail();
			model3_score = 0;
			return;
		}

	}
}


void init_role(Role* role, int x, int y)
{
	role->x = x;
	role->y = y;
	role->height = ROLE.getheight();
	role->width = ROLE.getwidth();
	role->score = 0;
	role->situ = Default;
}

void draw_role(Role* role)
{
	//// 绘制分数板(将图上的100覆盖掉)
	setfillcolor(RGB(231, 226, 244));
	solidrectangle(920, 112, 1003, 163);


	//用sprintf 绑定分数，关卡
	
	// 分数显示
	settextstyle(48, 0, "Consolas", 0, 0, 0, false, false, false);
	settextcolor(RGB(132, 132, 230));
	setbkmode(TRANSPARENT);

	char goal[100] = { 0 };
	sprintf_s(goal, "%d", role->score); //绑定人物分数

	//绘制人物分数
	int width = (83 - textwidth(goal)) / 2;
	int height = (51 - textheight(goal)) / 2;
	outtextxy(920 + width, 112 + height, goal);
	drawImg(910, 575, &GOLE);
	

	//绘制目标分数
	char strGoal[100] = { 0 };
	sprintf_s(strGoal, "%d",target_goal);
	width = (120 - textwidth(strGoal)) / 2;
	height = (50 - textheight(strGoal)) / 2;
	outtextxy(960 + width, 570 + height, strGoal);


	// 绘制关卡数
	setfillcolor(RGB(231, 226, 244));
	solidrectangle(930, 5, 993, 94);
	settextstyle(50, 0, "Consolas");

	char level[10] = { 0 };

	if (role->Mode == Endless) {
		sprintf_s(level, "%d", pass);
	}

	else 
	{
		sprintf_s(level, "%d/4", pass);	// 和关卡数绑定
	}

	width = (63 - textwidth(level)) / 2;
	height = (89 - textheight(level)) / 2;
	outtextxy(930 + width, 5 + height, level);

	//绘制人物
	drawImg(role->x, role->y, &ROLE);
}

//绘制矿石
void init_mine(Mine* mine, int x, int y,int price, int type)
{
	mine->height = (*mineTypes[type]).getheight();
	mine->width = (*mineTypes[type]).getwidth();
	mine->price = price;
	mine->x = x;
	mine->y = y;
	mine->type = type;
	mine->isCatch = false;
}

//所有的初始化
void init()
{
	init_role(&role, WIDTH / 2 - ROLE.getwidth() / 2, HEIGHT - 186);
	init_hook(&hook, role.x + 74, role.y + 44);

	//矿物进一步的初始化
	srand(time(NULL));

	for (int i = 0; i < mineNum; i++)
	{
		int x = rand() % (getwidth() - 87) + 20;	// 调整矿物生成位置
		int y = rand() % (getheight() - 240);
		int price = 10;

		int type = rand() % types;
		//可以设置各种类型

		switch (type)
		{
		case mine_1:
			price = rand() % 100 + 100;
			break;
		case mine_2:
			price = rand() % 120 + 100;
			break;
		case mine_3:
			price = rand() % 130 + 200;
			break;
		}

		init_mine(&mines[i],x, y, price, type);
	}

}

void draw_mine(Mine *mine)
{
	for (int i = 0; i < mineNum; i++)
	{
		if (mine[i].isCatch == false)
		{
			int index = mine[i].type;
			drawImg(mine[i].x, mine[i].y,mineTypes[index]);
		}
	}

}


// 初始化钩子
void init_hook(Hook* hook, int x, int y)
{
	hook->x = x;
	hook->y = y;
	hook->len = 70;
	hook->end_x = hook->x;
	hook->end_y = hook->y - hook->len;
	hook->angle = 0;
	hook->Dir = Left;	//初始增
	hook->state = Normal;	// 初始摆动
	hook->index = -1;	// 未抓取任何物品
}

// 绘制钩子
void draw_hook(Hook* hook)
{
	setlinestyle(PS_SOLID, 4);		// 设置线条样式
	setlinecolor(WHITE);
	line(hook->x, hook->y, hook->end_x, hook->end_y);
	// 钩子端点
	setfillcolor(WHITE);
	solidcircle(hook->end_x, hook->end_y, 3);
}

// 钩子摆动
void hook_swing(Hook* hook, double angleInc)
{
	if (hook->state == Normal)
	{
		if (hook->Dir)
		{
			// 改变角度(增)
			hook->angle += angleInc;
			if (hook->angle > MAX_ANGLE) hook->Dir = Left;
		}
		else
		{
			// 改变角度(减)
			hook->angle -= angleInc;
			if (hook->angle < -MAX_ANGLE) hook->Dir = Right;
		}
		// 更新 end_x, end_y
		hook->end_x = hook->x + hook->len * sin(hook->angle * PI / 180);	// 角度制转弧度制
		hook->end_y = hook->y - hook->len * cos(hook->angle * PI / 180);
	}
}


void hook_control(Hook* hook, double speed)
{

	// 在Normal状态 按空格 钩子伸长
	//& 0x 8000 注意这里位运算
	if (GetAsyncKeyState(VK_SPACE) & 0x8000 && hook->state == Normal)
	{
		hook->state = Long;	// 状态更新成Long
	}
	// 伸长状态
	if (hook->state == Long)
	{
		// 求dx, dy
		hook->dx = sin(hook->angle * PI / 180) * speed;
		hook->dy = cos(hook->angle * PI / 180) * speed;
		// 伸长
		hook->end_x += hook->dx;
		hook->end_y -= hook->dy;

		// 缩回判断 (碰到边界)
		if (hook->end_x <= 0 || hook->end_x >= getwidth() || hook->end_y <= 0) { hook->state = Short; }
	}

	if (hook->state == Short)
	{
		// 缩短
		hook->end_x -= hook->dx;
		hook->end_y += hook->dy;
		// 回到 Normal 状态	判断该情况为碰边界缩回
		//DISTANCE （勾股定理算长）
		if (DISTANCE(hook) <= hook->len && hook->index == -1) { hook->state = Normal; }
	}
}

//void hook_catch(Hook* hook)
//{
//	if (hook->state == Normal) { return; }
//
//	for (int i = 0; i < mineNum; i++)
//	{
//		//没有被抓取 
//		if (mines[i].isCatch == false && hook->end_x > mines[i].x
//			&& hook->end_x < mines[i].width + mines[i].x && hook->end_y > mines[i].y
//			&& hook->end_y < mines[i].y + mines[i].height)
//		{
//			//标识记录下标
//			hook->index = i;
//			break;
//		}
//	}
//  
 
// 这段代码有问题 抓到矿石，矿石没有消失

//	//抓取后处理
//	if (hook->index != -1)
//	{
//		hook->state = Short;
//
//		//将矿物带回，调整抓取位置（矿物被钩子钩住，hook->end_x变化，带着矿物的坐标也变动）
//		//设置矿物在左半屏的时候，抓取的点位是在右下(上)，矿物在右半屏的时候，抓取的点位在左下(上)
//
//		if (mines[hook->index].x < getwidth() / 2)
//		{
//			mines[hook->index].x = hook->end_x - mines[hook->index].width;
//
//		}
//		else
//		{
//			mines[hook->index].x = hook->end_x;
//		}
//
//		mines[hook->index].y = hook->end_y - mines[hook->index].height;
//		
//		if (DISTANCE(hook) < hook->len)
//		{
//			hook->state = Normal;
//			hook->index = -1; //钩子状态重置
//			mines[hook->index].isCatch = true;
//			//这里要加分数
//		}
//	}
//
//}

// 矿物抓取
void hook_catch(Hook* hook)
{

//goto 语句


	// Normal状态直接跳出
	if (hook->state == Normal) { return; }
	// 遍历判断
	for (int i = 0; i < mineNum; i++)
	{
		if (!mines[i].isCatch &&	// 未被抓取
			hook->end_x > mines[i].x && hook->end_x < mines[i].x + mines[i].width &&	// end_x在横向判定区域
			hook->end_y > mines[i].y && hook->end_y < mines[i].y + mines[i].height)	// end_y在竖向判定区域
		{
			hook->index = i;	// 记录抓取到的矿物标识
			break;	// 找到后退出循环
		}
	}
	// 抓取后处理
	if (hook->index != -1)
	{
		hook->state = Short;	// 钩子状态更新为缩短
		// 将矿物带回,可调整抓取位置
		if (mines[hook->index].x < getwidth() / 2) {
			mines[hook->index].x = hook->end_x - mines[hook->index].width;
		}	// 判断的意义是，矿物在左半屏时抓取点定位在右下，在右半屏时抓取点定位在左下
		else 
		{
			mines[hook->index].x = hook->end_x;
		}

		mines[hook->index].y = hook->end_y - mines[hook->index].height;

		// 缩回至正常状态时
		if (DISTANCE(hook) < hook->len)
		{
			hook->state = Normal;
			mines[hook->index].isCatch = true;		// 更新矿物状态
			role.score += mines[hook->index].price;		// 更新角色分数
			hook->index = -1;	//  钩子状态重置
		}
	}
}

void Judge(Role* role)
{
	if (role->Mode == Normal)
	{
		if (role->score >= target_goal)
		{
			if (pass < 4)
			{
				role->situ = notFinish;
				pass++;
				target_goal += 300;
			}
			else
			{
				role->situ = Win;
				pass = 1;
				target_goal = 400;
			}
		}
	}

	if (role->Mode == Challenge)
	{
		//看分数是否达标
		if (role->score >= target_goal)
		{
			if (pass < 4)
			{
				role->situ = notFinish;
				pass += 1;
				target_goal += 250;
				left_time = LEFT_TIME;
			}
			else
			{
				role->situ = Win;
				pass = 1;
				target_goal = 400;
				left_time = left_time;
			}
		}

		//倒计时，是否为0，为0为输
		if (left_time == 0)
		{
			
			role->situ = Fail;
			pass = 1;
			target_goal = 400;
			left_time = LEFT_TIME;
		}
	}

	if (role->Mode == Endless)
	{
		if (role->score >= target_goal)
		{
			role->situ = notFinish;
			pass ++;
			left_time += 5;
			target_goal += 400;
			mode3_score += role->score;
		}

		if (left_time == 0)
		{
			role->situ = End;
			pass = 1;
			left_time = LEFT_TIME;
			model3_score += role->score;
			target_goal = 400;
		}
	}
}


void draw_win()
{
	initgraph(WIDTH, HEIGHT);
	putimage(0, 0, &WIN);

	LPBTN exit = createButton(946, 0, 1080, 50, RGB(201, 179, 158), RGB(161, 139, 118), "返回选关");
	draw_button0(exit);

	ExMessage m;
	while (true)
	{
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角
			solidrectangle(946, 0, 1080, 50);
			draw_button1(exit);
			if (m.lbutton == true) 
			{ 
				return mainMenu(); 
			}
		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角
			solidrectangle(946, 0, 1080, 50);
			draw_button0(exit);
		}

		EndBatchDraw();
	}

}

void draw_fail()
{
	initgraph(WIDTH, HEIGHT);
	putimage(0, 0, &FAIL);

	LPBTN exit = createButton(946, 0, 1080, 50, RGB(193, 178, 155), RGB(153, 138, 115), "返回选关");
	draw_button0(exit);

	ExMessage m;
	while (true)
	{
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角
			solidrectangle(946, 0, 1080, 50);
			draw_button1(exit);

			if (m.lbutton == true) 
			{
				return mainMenu();
			}

		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角
			solidrectangle(946, 0, 1080, 50);

			draw_button0(exit);
		}
		EndBatchDraw();
	}

}


//倒计时，(用多线程实现倒计时)
void countDown(void* pArg)
{
	left_time = *(int*)pArg;
	do
	{
		Sleep(1000);//windows.h头文件
		left_time -= 1;

		if (role.situ != Default && role.situ != notFinish)
		{
			_endthread(); //线程结束  windows.h头文件
		}
	} while (left_time >= 1);

	_endthread();
}

void draw_time(int t)   //倒计时绘制
{
	// 图标
	drawImg(910, 635, &TIME);
	settextstyle(50, 0, "Consolas");
	settextcolor(RGB(132, 132, 230));
	setbkmode(TRANSPARENT);

	char time[15] = { 0 };
	sprintf_s(time, "%d", t); //与时间进行绑定

	int width = (83 - textwidth(time)) / 2;
	int height = (51 - textheight(time)) / 2;
	outtextxy(960 + width, 630 + height, time);

}

void draw_end()
{

	initgraph(WIDTH, HEIGHT);
	putimage(0, 0, &WIN);

	settextcolor(RGB(14, 20, 18));
	settextstyle(50, 0, "Consolas");
	setbkmode(TRANSPARENT);
	char str[20] = { 0 };
	sprintf_s(str, "总得分: %d", mode3_score);
	outtextxy(509, 63, str);

	LPBTN exit = createButton(946, 0, 1080, 50, RGB(201, 179, 158), RGB(161, 139, 118), "返回选关");
	draw_button0(exit);


	ExMessage m;
	while (true)
	{
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角

			solidrectangle(946, 0, 1080, 50);
			draw_button1(exit);
			if (m.lbutton == true) 
			{
				return mainMenu(); 
			}
		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角

			solidrectangle(946, 0, 1080, 50);
			draw_button0(exit);
		}
		EndBatchDraw();
	}
}


void rule_menu()
{
	IMAGE RULE;
	loadimage(&RULE, "./src/img/rules.png", WIDTH, HEIGHT);

	putimage(0, 0, &RULE);

	LPBTN exit = createButton(0, 113, 128, 141, RGB(144, 150, 239), RGB(104, 110, 199), "返回菜单");
	draw_button0(exit);

	ExMessage m;
	while (true)
	{
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		// 返回交互
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {0, 0}, {0, 113}, {128, 113} };
			solidpolygon(pts1, 3);	// 上三角
			POINT pts2[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts2, 3);	// 下三角
			solidrectangle(0, 113, 128, 141);
			draw_button1(exit);

			

			if (m.lbutton == true) { return mainMenu(); }
		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {0, 0}, {0, 113}, {128, 113} };
			solidpolygon(pts1, 3);
			POINT pts2[] = { {0, 141}, {128, 141}, {128, 255} };
			solidpolygon(pts2, 3);
			solidrectangle(0, 113, 128, 141);
			draw_button0(exit);

			
		}
		EndBatchDraw();
	}

}

void story_menu()
{
	IMAGE STORY;
	loadimage(&STORY, "./src/img/story.jpg", WIDTH, HEIGHT);
	putimage(0, 0, &STORY);

	LPBTN exit = createButton(946, 0, 1080, 50, RGB(196, 144, 69), RGB(156, 104, 29), "返回");
	draw_button0(exit);

	ExMessage m;
	while (true)
	{
		m = getmessage(EX_MOUSE);
		BeginBatchDraw();
		if (m.x >= exit->x && m.x <= exit->xx && m.y >= exit->y && m.y <= exit->yy)
		{
			setfillcolor(exit->color1);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角

			solidrectangle(946, 0, 1080, 50);
			draw_button1(exit);
			if (m.lbutton == true) { return mainMenu(); }
		}
		else {
			setfillcolor(exit->color0);
			POINT pts1[] = { {800, 0}, {946, 0}, {946, 50} };
			solidpolygon(pts1, 3);	// 左三角
			POINT pts2[] = { {946, 50}, {1080, 50}, {1080, 96} };
			solidpolygon(pts2, 3);	// 下三角

			solidrectangle(946, 0, 1080, 50);
			draw_button0(exit);
		}
		EndBatchDraw();
	}
}